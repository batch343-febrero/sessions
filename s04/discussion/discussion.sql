SELECT * FROM songs WHERE song_name != "Kundiman";

SELECT song_name, length FROM songs WHERE genre != "OPM";

SELECT * FROM songs WHERE  length > 300;

SELECT * FROM songs WHERE  length < 300;

SELECT * FROM songs WHERE  length > 300 AND length < 400;

SELECT * FROM songs WHERE  length >= 300 AND length <= 416;

SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 3; 

SELECT * FROM songs WHERE id IN (1, 3, 5); 

SELECT * FROM songs WHERE genre IN ("POP", "Pop rock");

SELECT * FROM songs WHERE song_name LIKE "%story";

SELECT * FROM songs WHERE song_name LIKE "k%";

SELECT * FROM songs WHERE song_name LIKE "s%e";

SELECT * FROM songs WHERE song_name LIKE "%r%";

SELECT * FROM songs WHERE song_name LIKE "%s%o%";

SELECT * FROM songs WHERE song_name LIKE "l%r%" AND genre = "Pop";

SELECT* FROM albums WHERE date_released LIKE "201_";

SELECT* FROM albums WHERE date_released LIKE "201%";

SELECT* FROM albums WHERE album_title LIKE "Tr_p";

SELECT* FROM albums WHERE album_title LIKE "Tr%";

SELECT * FROM songs WHERE id LIKE "1_";

SELECT * FROM songs WHERE id LIKE "_";

SELECT * FROM songs ORDER BY song_name ASC;

SELECT * FROM songs WHERE song_name LIKE "%s%o%" ORDER BY song_name ASC;

SELECT * FROM artists 
	JOIN albums 
	ON artists.id = albums.artist_id;

SELECT artists.name, albums.album_title, albums.date_released 
	FROM artists 
	JOIN albums 
	ON artists.id = albums.artist_id;

SELECT * FROM artists JOIN albums 
	ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name  FROM artists JOIN albums 
	ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name  FROM artists JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id 
	WHERE song_name LIKE "l%" 
	ORDER BY song_name ASC;